# opengeoweb-demo

This project demonstrates how to re-use opengeoweb components from https://www.npmjs.com/package/@opengeoweb/core. It contains a storybook, application and a build script.

![image](docs/opengeoweb-demo.png)

## Install dependencies:

```
nvm use 20
npm ci
```

## To run the storybook:

In this case the files in folder `stories` are used.

`npm run storybook`

It should automatically show in your browser.

## To run the application:

In this case the files in the folder `src/application` are used.

`npm run start`

and then visit the url http://localhost:4000/ in your browser.

## To build a application

A compiled version using only a javascript and a index.html file can be made with:

`npm run build`

The application is in that case written to the `dist` folder. You run the application in the dist folder via

`npx serve dist`

and then visit the url http://localhost:3000/ in your browser.

## To test a build locally

If you want to test out a geoweb lib locally in any external repository, use the following steps:

- make your needed changes in the `opengeoweb` repository inside a libray. For example the `shared` lib.
- when you're happy with the changes, make a build by `nx build shared`
- go to the `dist` folder of shared (`dist/libs/shared`), and make a npm package by using `npm pack`. Copy the path of this newly generated package
- go back the external library where you want to try out the new package; in our case `opengeoweb-demo` repository
- install the pacakge by `npm install '{path-to.tgz}'`
- run the application or storybook to test the changes out
