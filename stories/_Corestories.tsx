/* eslint-disable react/jsx-props-no-spreading */
import * as React from 'react';
import {
  MapControls,
  defaultLayers,
  publicLayers,
} from '@opengeoweb/webmap-react';
import {
  DemoWrapperConnect,
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
  LegendConnect,
  LegendMapButtonConnect,
  MapViewConnect,
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
  TimeSliderConnect,
} from '@opengeoweb/core';
import { useDefaultMapSettings } from '@opengeoweb/webmap-redux';
import { createToolkitMockStore } from '@opengeoweb/shared';

const MapWithLayerManager: React.FC<{ mapId: string }> = ({ mapId }) => {
  useDefaultMapSettings({
    mapId,
    layers: [
      { ...publicLayers.radarLayer, id: `radar-${mapId}` },
      { ...publicLayers.harmonieWindPl, id: `harmonieWindPl-${mapId}` },
      { ...publicLayers.harmonieAirTemperature, id: `temp-${mapId}` },
    ],
    baseLayers: [
      { ...defaultLayers.baseLayerGrey, id: `baseGrey-${mapId}` },
      defaultLayers.overLayer,
    ],
  });

  return (
    <div style={{ height: '100vh' }}>
      <LayerManagerConnect />
      <MapControls style={{ top: 112, left: 16 }}>
        <LayerManagerMapButtonConnect mapId={mapId} />
        <LegendMapButtonConnect mapId={mapId} />
        <MultiDimensionSelectMapButtonsConnect mapId={mapId} />
      </MapControls>

      <LegendConnect mapId={mapId} />
      <MultiMapDimensionSelectConnect />
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 50,
          width: '100%',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
      <MapViewConnect mapId={mapId} />
    </div>
  );
};

const createMockStore = () =>
  createToolkitMockStore({
    reducer: {},
    preloadedState: {},
    middleware: [],
  });

const store = createMockStore();

// TODO: Currently broken due to https://gitlab.com/opengeoweb/geoweb-assets/-/issues/3796
export const LayerManagerConnectLightTheme = (): React.ReactElement => (
  <DemoWrapperConnect store={store}>
    <MapWithLayerManager mapId="mapid_1" />
  </DemoWrapperConnect>
);

export default {
  title: 'demo/core',
};
