/* eslint-disable react/jsx-props-no-spreading */
import * as React from 'react';
import Box from '@mui/material/Box';
import { MapView, MapViewLayer } from '@opengeoweb/webmap-react';
import { LayerType } from '@opengeoweb/webmap';
import { ThemeWrapper } from '@opengeoweb/theme';
import { I18nextProvider } from 'react-i18next';
import { DemoWMSViewer } from '../src/components/DemoWMSViewer';
import { baseLayerWorldMap } from '../src/utils/layerDefinitions';
import i18n from '../src/utils/i18n';

const DemoWrapper: React.FC<{ children: React.ReactNode }> = ({ children }) => (
  <I18nextProvider i18n={i18n}>
    <ThemeWrapper>{children}</ThemeWrapper>
  </I18nextProvider>
);

export const DemoDemoWMSViewerStory = (): React.ReactElement => {
  return (
    <DemoWrapper>
      <Box>
        <DemoWMSViewer />
      </Box>
    </DemoWrapper>
  );
};

DemoDemoWMSViewerStory.storyName = 'WMSViewer';

export const MapComponentDemo = (): React.ReactElement => {
  return (
    <DemoWrapper>
      <div style={{ width: '800px', height: '800px' }}>
        <MapView
          mapId="demo"
          shouldAutoFetch={false}
          srs="EPSG:28992"
          bbox={{
            left: -409921,
            bottom: -32731,
            right: 692578,
            top: 1069768,
          }}
        >
          <MapViewLayer {...baseLayerWorldMap} />
          <MapViewLayer
            service="https://geoservices.knmi.nl/adaguc-server?DATASET=knmi_animated_gif_baselayers&"
            name="knmi_animated_gif_background_europe_elevation_1km_combined"
            id="knmi_animated_gif_baselayers_1"
          />

          <MapViewLayer
            id="radarlayer"
            service="https://geoservices.knmi.nl/adagucserver?dataset=RADAR&"
            name="Reflectivity"
          />
          {/* <MapViewLayer
            id="yourlayer"
            service="https://<yourhost>/adagucserver?dataset=RAD_NL25_PCP_CM&"
            name="REFLECTIVITY"
          /> */}
          <MapViewLayer
            id="overlay"
            service="https://geoservices.knmi.nl/adagucserver?dataset=baselayers&"
            name="overlay_europe"
            layerType={LayerType.overLayer}
          />
        </MapView>
      </div>
    </DemoWrapper>
  );
};

MapComponentDemo.storyName = 'Map';

export default {
  title: 'demo/webmap-react',
};
