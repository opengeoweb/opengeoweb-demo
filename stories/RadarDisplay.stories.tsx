/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable no-void */
/* eslint-disable react/style-prop-object */
/* eslint-disable react/jsx-props-no-spreading */
import * as React from 'react';
import { MapView, MapViewLayer } from '@opengeoweb/webmap-react';
import { getWMLayerById } from '@opengeoweb/webmap';
import { ThemeWrapper } from '@opengeoweb/theme';
import { I18nextProvider } from 'react-i18next';
import { baseLayerWorldMap } from '../src/utils/layerDefinitions';
import i18n from '../src/utils/i18n';

const DemoWrapper: React.FC<{ children: React.ReactNode }> = ({ children }) => (
  <I18nextProvider i18n={i18n}>
    <ThemeWrapper>{children}</ThemeWrapper>
  </I18nextProvider>
);

let animSequence = 0;
const numAnimSteps = 20;

export const RadarDisplayDemo = (): React.ReactElement => {
  const radarLayer = {
    id: 'radarlayer',
    service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
    name: 'RAD_NL25_PCP_CM',
    style: 'precip/nearest',
  };
  React.useEffect(() => {
    const interval = setInterval(() => {
      const layer = getWMLayerById(radarLayer.id);
      layer.parseLayer().then(() => {
        const timeDim = layer.getDimension('time');
        if (timeDim) {
          const numSteps = timeDim.size();
          const timeValue = timeDim.getValueForIndex(
            numSteps - numAnimSteps + animSequence,
          ) as string;
          timeDim.setValue(timeValue);
          layer.parentMap.draw();
        }
      });
      animSequence += 1;
      if (animSequence > numAnimSteps) {
        animSequence = 0;
      }
    }, 100);
    return () => clearInterval(interval);
  }, []);
  return (
    <DemoWrapper>
      <div style={{ width: '800px', height: '800px' }}>
        <MapView
          mapId="demo"
          shouldAutoFetch={false}
          srs="EPSG:28992"
          showLegend
          bbox={{
            left: -409921,
            bottom: -32731,
            right: 692578,
            top: 1069768,
          }}
        >
          <MapViewLayer {...baseLayerWorldMap} />
          <MapViewLayer
            service="https://geoservices.knmi.nl/adaguc-server?DATASET=knmi_animated_gif_baselayers&"
            name="knmi_animated_gif_background_europe_elevation_1km_combined"
            id="knmi_animated_gif_baselayers_1"
          />

          <MapViewLayer {...radarLayer} />
          {/* <MapViewLayer
            id="overlay"
            service="https://geoservices.knmi.nl/adagucserver?dataset=baselayers&"
            name="overlay_europe"
            layerType={LayerType.overLayer}
          /> */}
        </MapView>
      </div>
    </DemoWrapper>
  );
};

RadarDisplayDemo.storyName = 'RadarDisplay';

export default {
  title: 'demo/webmap-react',
};
