import 'regenerator-runtime/runtime';
import React from 'react';
import { DemoWMSViewer } from '../components/DemoWMSViewer';
import { AppWrapper } from '../components/AppWrapper';

const App = (): React.ReactElement => {
  // Note: The used config variable is a global var defined in index.html
  return (
    <AppWrapper>
      <div
        style={{
          background: 'white',
          width: '100vw',
          height: '100vh',
          margin: 0,
          padding: 20,
        }}
      >
        <DemoWMSViewer />
      </div>
    </AppWrapper>
  );
};

export default App;
