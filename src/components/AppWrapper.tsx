import { I18nextProvider } from 'react-i18next';
import { ThemeWrapper } from '@opengeoweb/theme';
import React from 'react';
import i18n from '../utils/i18n';

export const AppWrapper: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  return (
    <I18nextProvider i18n={i18n}>
      <ThemeWrapper>{children}</ThemeWrapper>;
    </I18nextProvider>
  );
};

export default AppWrapper;
