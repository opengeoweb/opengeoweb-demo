/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable react/jsx-props-no-spreading */
import {
  WMLayer,
  LayerProps,
  generateLayerId,
  registerWMLayer,
  LayerType,
  LayerFoundation,
} from '@opengeoweb/webmap';

const selectLayer = (
  selectedLayer: LayerProps,
  wmsService: string,
): Promise<LayerFoundation> => {
  return new Promise((resolveLayer) => {
    const wmsLayer = {
      service: wmsService,
      name: selectedLayer.name,
      id: generateLayerId(),
      format: 'image/webp',
      enabled: true,
      layerType: LayerType.mapLayer,
    };
    const wmLayer = new WMLayer(wmsLayer);
    registerWMLayer(wmLayer, wmsLayer.id);
    wmLayer.parseLayer().then(() => {
      resolveLayer(wmsLayer);
    });
  });
};
export default selectLayer;
