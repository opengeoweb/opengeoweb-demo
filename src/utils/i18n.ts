import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import {
  webmapReactTranslations,
  WEBMAP_REACT_NAMESPACE,
} from '@opengeoweb/webmap-react';
import { coreTranslations, CORE_NAMESPACE } from '@opengeoweb/core';
import { sharedTranslations, SHARED_NAMESPACE } from '@opengeoweb/shared';
import {
  timesliderTranslations,
  TIMESLIDER_NAMESPACE,
} from '@opengeoweb/timeslider';

// eslint-disable-next-line @typescript-eslint/no-floating-promises
i18n.use(initReactI18next).init({
  lng: 'en',
  fallbackLng: 'en',
  resources: {
    en: {
      [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.en,
      [CORE_NAMESPACE]: coreTranslations.en,
      [SHARED_NAMESPACE]: sharedTranslations.en,
      [TIMESLIDER_NAMESPACE]: timesliderTranslations.en,
    },
    no: {
      [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.no,
    },
    fi: {
      [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.fi,
    },
    nl: {
      [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.nl,
    },
  }, // load non-library specific translations here
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
